import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import CartContext from "../../store/cart-context";
import UserContext from "../../store/user-context";
import LogoWhite from "../../assets/images/logoWhite.png";
import "./CheckOutPage.css";

import { v4 as uuid } from "uuid";

const CheckOutPage = (props) => {
  const navigate = useNavigate();
  const unique_id = uuid();
  const small_id = unique_id.slice(0, 8);
  const { products, deleteProductsArrayHandler } = useContext(CartContext);
  const { user } = useContext(UserContext);
  const [total, setTotal] = useState(0);

  useEffect(() => {
    let x = 0;
    for (const product of products) {
      x += +product.cartQuantity * +product.price;
    }
 
    setTotal(x);

  }, [products]);

  const placeOrderHandler = async () => {
    if (!user.address) {
      Swal.fire({
        title: "Update Address",
        icon: "error",
        text: "Please Update your shipping address!",
      });
      return;
    }

    if (user.id === null) {
      Swal.fire({
        title: "Authentication Failed",
        icon: "error",
        text: "Please login to checkout order!",
      });
      return;
    }

    if (user.isAdmin === true) {
      Swal.fire({
        title: "Authentication Failed",
        icon: "error",
        text: "Admins are not allowed to take order!",
      });
      return;
    }

    const token = localStorage.getItem("token");
    console.log("check products");
    console.log(products);
    const newProducts = products.map((product) => {
      return {
        productId: product.productId,
        price: +product.price,
        quantity: +product.cartQuantity,
        imgPath: product.imgPath,
        tracking_number: small_id,
        name: product.name
      };
    });
    console.log('new products');
    console.log(newProducts);
    const response = await fetch(`${process.env.REACT_APP_API_URL}/orders/`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "content-type": "application/json",
      },
      body: JSON.stringify({
        products: newProducts,
      }),
    });
    
    const jsonReponse = await response.json();
    console.log('why insufficient stock');
    console.log(jsonReponse);

    console.log(jsonReponse);
    if (jsonReponse.productQuantity === 0) {
      Swal.fire({
        title: "Out of Stock",
        icon: "error",
        text: "Some users may have checked-out this product! Please check on another day for re-stocking",
      });
      return;
    }

    if (jsonReponse.productQuantity !== 0) {
      Swal.fire({
        title: "Insufficient Stock",
        icon: "error",
        text: "Some users may have checked-out this product! Please adjust your order according to the new stock!",
      });
      // setQuantity(jsonReponse.productQuantity);
    }
    if (jsonReponse.orderSuccess) {
      Swal.fire({
        title: "Order Received",
        icon: "success",
        text: "Your order was received.A customer service representative will call you shortly. You will now be redirected to the products page",
      });
      // setQuantity(jsonReponse.productQuantity);
      deleteProductsArrayHandler();
      navigate("/shop");
      return;
    }
  };

  const updateAddressHandler = () => {
    navigate("/user/address");
  };

  const backToCartHandler = () =>{
    navigate("/cart");
  }
  return (
    <div className="container mt-5 mb-5">
      <div className="row justify-content-center">
        <div className="col-md-12 col-lg-10">
          <div className="wrap">
            <div>
              <div className="text-wrap  form-wrap py-3 text-center  align-items-center order-md-last">
                <div className="text w-100">
                  <img src={LogoWhite} alt="" className="form-img" />
                </div>
              </div>
              <div className="pt-3">
                <p className="text-center cart-heading">
                  {products.length > 0 ? "Order Details" : "No Order"}
                </p>
                <hr className="cart-hr"></hr>
              </div>
              {/* Start here */}
              <div className="container">
                <div className="row">
                  <div className="col-md-6">
                    <ul>
                      <li className="order-label pt-2">SUMMARY</li>
                      <ul className="d-flex">
                        <li className="summary-list">Tracking #:</li>
                        <li className="summary-description">{small_id}</li>
                      </ul>

                      <ul className="d-flex">
                        <li className="summary-list">Order Date:</li>
                        <li className="summary-description">
                          {new Date().toLocaleDateString()}
                        </li>
                      </ul>
                      <ul className="d-flex">
                        <li className="summary-list">Order Total:</li>
                        <li className="summary-description">
                          {total.toFixed(2)} PHP
                        </li>
                      </ul>
                    </ul>
                  </div>
                  <div className="col-md-6">
                    <ul>
                      <li className="order-label pt-2">SHIPPING ADDRESS</li>
                      <li>
                        {!user.address
                          ? "No Address Provided. Please update address"
                          : user.address}
                      </li>
                      <li>
                        <div className="form-group pt-2">
                          <p className="cart-p">
                            <button
                              type="button"
                              className="form-control btn btn-primary submit px-3 cart-remove-btn"
                              onClick={updateAddressHandler}
                            >
                              Update
                            </button>
                          </p>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                <hr className="cart-hr"></hr>
                <div className="row px-3">
                  <div className="card orders-card mb-2">
                    <div className="card-body">
                      <table className="table table-borderless table-orders">
                        <thead>
                          <tr>
                            <th scope="col table-orders-header">
                              ITEMS ORDERED
                            </th>
                            <th scope="col">NAME</th>
                            <th scope="col">QUANTITY</th>
                            <th scope="col">AMOUNT</th>
                          </tr>
                        </thead>
                        <tbody>
                          {products.map((product) => (
                            <tr key={product.productId}>
                              <td>
                                <img
                                  src={`${process.env.REACT_APP_API_URL}/${product.imgPath}`}
                                  alt=""
                                  className="orders-img"
                                />
                              </td>
                              <td>{product.name}</td>
                              <td>{product.cartQuantity}</td>
                              <td>
                                {(
                                  +product.cartQuantity * +product.price
                                ).toFixed(2)}{" "}
                                PHP
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <hr className="cart-hr"></hr>
                <div className="row px-3">
                  <div className="card orders-card mb-3">
                    <div className="card-body">
                      <div className="row">
                        <div className="col-md-6">
                          <ul>
                            <li className="order-label pt-2">
                              IMPORTANT NOTICE:
                            </li>
                            <li>
                              As of the moment, we only do COD transaction.
                              Delivery is every weekend(Fri, Sat, Sun) of the
                              current week. A customer agent will call you right
                              after you place the order.
                            </li>
                          </ul>
                        </div>
                        <div className="col-md-6">
                          <ul className="d-flex justify-content-end pt-1">
                            <li className="order-bottom-summary-list">
                              <strong>{`SubTotal (${products.length} Items):`}</strong>
                            </li>
                            <li className="order-bottom-summary-description">
                              {total.toFixed(2)} PHP
                            </li>
                          </ul>
                          <ul className="d-flex justify-content-end">
                            <li className="order-bottom-summary-list">
                              <strong>Shipping Fee:</strong>
                            </li>
                            <li className="order-bottom-summary-description">
                              FREE
                            </li>
                          </ul>
                          <ul className="d-flex justify-content-end">
                            <li className="order-bottom-summary-list">
                              <strong>Discount:</strong>
                            </li>
                            <li className="order-bottom-summary-description">
                              0.00 PHP
                            </li>
                          </ul>
                          <ul className="d-flex justify-content-end">
                            <li className="order-bottom-summary-list text-danger">
                              <strong>Order Total:</strong>
                            </li>
                            <li className="order-bottom-summary-description text-danger">
                              <strong>{total.toFixed(2)} PHP</strong>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="row pt-5">
                 
                        <div className="col">
                          <div className="form-group ">
                            <p className="cart-p">
                            <button
                                type="button"
                                className="form-control btn btn-primary submit px-3 cart-remove-btn mx-3 my-3"
                                onClick={backToCartHandler}
                              >
                                Back to Cart
                              </button>
                              <button
                                type="button"
                                className="form-control btn btn-primary submit px-3 cart-remove-btn mx-3"
                                onClick={placeOrderHandler}
                              >
                                Place Order
                              </button>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {/* end here */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CheckOutPage;
