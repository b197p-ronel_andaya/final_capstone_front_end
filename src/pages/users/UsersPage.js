import React, { useEffect, useState } from "react";
import User from "../../components/user/User";
import './UsersPage.css'
const UsersPage = (props) => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    async function fetchUsers() {
      const token = localStorage.getItem("token");
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/users/getAll`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
            "content-type": "application/json",
          },
        }
      );

      const jsonReponse = await response.json();
      setUsers(jsonReponse);
    }
    fetchUsers();
  }, []);

  return (      <div class="container py-5">

  <div className="row">
            <div className="card orders-card mb-2">
              <div className="card-body">
                <h4 className="text-center">Users List</h4>
                <hr className="cart-hr"/>
                <table class="table table-borderless table-orders">
                  <thead>
                    <tr>
                      <th scope="col">FIRST NAME</th>
                      <th scope="col">LAST NAME</th>
                      <th scope="col">EMAIL</th>
                      <th scope="col">CONTACT #</th>
                      <th scope="col">ADMIN</th>
                      <th scope="col set-admin-action">ACTION</th>
                    </tr>
                  </thead>
                  <tbody>
                    {users.map((user) => (
                      <User key={user._id} user={user} />
                        
                      
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
  </div>)

};

export default UsersPage;
