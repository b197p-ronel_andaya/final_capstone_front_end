import React, { useContext } from "react";
import CartContext from "../../store/cart-context";
import Cart from "../../components/cart/Cart";
import CartSummary from "../../components/cart/CartSummary";
import LogoWhite from "../../assets/images/logoWhite.png";
import "./CartPage.css";
const CartPage = () => {
  const { products } = useContext(CartContext);
  console.log("You are on cart page and lets see products value");
  console.log(products);
  return (
    <div className="container mt-5 mb-5">
      <div className="row justify-content-center">
        <div className="col-md-12 col-lg-10">
          <div className="wrap">
            <div>
              <div className="text-wrap  form-wrap py-3 text-center  align-items-center order-md-last">
                <div className="text w-100">
                  <img src={LogoWhite} alt="" className="form-img" />
                </div>
              </div>
              <div className="pt-3">
                <p className="text-center cart-heading">
                  {products.length > 0 ? "Cart Orders" : "Cart Empty"}
                </p>
                <hr className="cart-hr"></hr>
              </div>
              {products.map((product) => (
                <Cart key={product.productId} value={product} />
              ))}
              {products.length > 0 ? <CartSummary /> : ""}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CartPage;
