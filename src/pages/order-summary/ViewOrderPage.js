import React, { useEffect, useState, useContext } from "react";
import { useNavigate, useParams } from "react-router-dom";
import UserContext from "../../store/user-context";
import Swal from "sweetalert2";

import LogoWhite from "../../assets/images/logoWhite.png";



const ViewOrderPage = (props) => {
const {user} = useContext(UserContext);
const navigate = useNavigate();
const [orderId, setOrderId] =  useState(useParams());
const [trackingNumber, setTrackingNumber] = useState("");
const [orderDate, setOrderDate] = useState("");
const [totalAmount, setTotalAmount] = useState(0);
const [totalCount, setTotalCount] = useState("");
const [newProducts, setNewProducts] = useState([]);
const [cancel, setCancel] = useState(false);
const [delivered, setDelivered] = useState(false);

useEffect(() =>{
    async function fetchOrder(){
        
        const token = localStorage.getItem("token");
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/orders/${orderId.orderId}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
            "content-type": "application/json",
          }
        }
      );
      const jsonReponse = await response.json();
      console.log(jsonReponse[0]);
      setTrackingNumber(jsonReponse[0].products[0].tracking_number);
      setOrderDate(jsonReponse[0].purchased_on);
      setTotalAmount(jsonReponse[0].total_amount);
      setCancel(jsonReponse[0].is_cancelled);
      setDelivered(jsonReponse[0].delivered)

      const newProducts = [];
      let counter = 0;
      for(const product of jsonReponse[0].products){
        counter++;
        newProducts.push(product);
      }
      setTotalCount(counter);
      setNewProducts(newProducts);
    }
    fetchOrder();
    
},[orderId.orderId])
const backToSummaryHandler = () => {
    navigate("/orders/summary")
}

const cancelOrderHandler = async () => {
    const token = localStorage.getItem("token");
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/orders/cancelled`,
      {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "content-type": "application/json",
        },
        body: JSON.stringify({
            orderId: orderId.orderId
        })
      }
    );
    const jsonReponse = await response.json();
    console.log(jsonReponse);

    if(jsonReponse.isCancelled){
        Swal.fire({
            title: "Order Cancelled",
            icon: "success",
            text: "Order cancelled! You will now be redirected to your orders summary.",
          });
          navigate("/orders/summary")
    }
}
  return (
    <div className="container mt-5 mb-5">
      <div className="row justify-content-center">
        <div className="col-md-12 col-lg-10">
          <div className="wrap">
            <div>
              <div className="text-wrap  form-wrap py-3 text-center  align-items-center order-md-last">
                <div className="text w-100">
                  <img src={LogoWhite} alt="" className="form-img" />
                </div>
              </div>
              <div className="pt-3">
                <p className={cancel ? "text-center cart-heading text-danger":"text-center cart-heading"}>
                    Order Details {cancel ? '(this order was cancelled)': (delivered ? '(order delivered)' : '')}
                </p>
                <hr className="cart-hr"></hr>
              </div>
              {/* Start here */}
              <div className="container">
                <div className="row">
                  <div className="col-md-6">
                    <ul>
                      <li className="order-label pt-2">SUMMARY</li>
                      <ul className="d-flex">
                        <li className="summary-list">Tracking #:</li>
                        <li className="summary-description">{trackingNumber}</li>
                      </ul>

                      <ul className="d-flex">
                        <li className="summary-list">Order Date:</li>
                        <li className="summary-description">
                          {new Date(orderDate).toLocaleDateString()}
                        </li>
                      </ul>
                      <ul className="d-flex">
                        <li className="summary-list">Order Total:</li>
                        <li className="summary-description">
                       {totalAmount.toFixed(2)} PHP
                        </li>
                      </ul>
                    </ul>
                  </div>
                  <div className="col-md-6">
                    <ul>
                      <li className="order-label pt-2">SHIPPING ADDRESS</li>
                      <li>
                        {user.address}
                      </li>
                    </ul>
                  </div>
                </div>
                <hr className="cart-hr"></hr>
                <div className="row px-3">
                  <div className="card orders-card mb-2">
                    <div className="card-body">
                      <table className="table table-borderless table-orders">
                        <thead>
                          <tr>
                            <th scope="col table-orders-header">
                              ITEMS ORDERED
                            </th>
                            <th scope="col">NAME</th>
                            <th scope="col">QUANTITY</th>
                            <th scope="col">AMOUNT</th>
                          </tr>
                        </thead>
                        <tbody>
                          {newProducts.map((product) => (
                            <tr key={product.product_id}>
                              <td>
                                <img
                                  src={`${process.env.REACT_APP_API_URL}/${product.img_path}`}
                                  alt=""
                                  className="orders-img"
                                />
                              </td>
                              <td>{product.name}</td>
                              <td>{product.quantity}</td>
                              <td>
                                {(+product.price * +product.quantity).toFixed(2)} PHP
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <hr className="cart-hr"></hr>
                <div className="row px-3">
                  <div className="card orders-card mb-3">
                    <div className="card-body">
                      <div className="row">
                        <div className="col-md-6">
                          <ul>
                            <li className="order-label pt-2">
                              IMPORTANT NOTICE:
                            </li>
                            <li>
                              As of the moment, we only do COD transaction.
                              Delivery is every weekend(Fri, Sat, Sun) of the
                              current week. A customer agent will call you right
                              after you place the order.
                            </li>
                          </ul>
                        </div>
                        <div className="col-md-6">
                          <ul className="d-flex justify-content-end pt-1">
                            <li className="order-bottom-summary-list">
                              <strong>{`SubTotal (${totalCount} Items):`}</strong>
                            </li>
                            <li className="order-bottom-summary-description">
                             {totalAmount.toFixed(2)} PHP
                            </li>
                          </ul>
                          <ul className="d-flex justify-content-end">
                            <li className="order-bottom-summary-list">
                              <strong>Shipping Fee:</strong>
                            </li>
                            <li className="order-bottom-summary-description">
                              FREE
                            </li>
                          </ul>
                          <ul className="d-flex justify-content-end">
                            <li className="order-bottom-summary-list">
                              <strong>Discount:</strong>
                            </li>
                            <li className="order-bottom-summary-description">
                              0.00 PHP
                            </li>
                          </ul>
                          <ul className="d-flex justify-content-end">
                            <li className="order-bottom-summary-list text-danger">
                              <strong>Order Total:</strong>
                            </li>
                            <li className="order-bottom-summary-description text-danger">
                              <strong>{totalAmount.toFixed(2)} PHP</strong>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="row pt-5">
                 
                        <div className="col">
                          <div className="form-group ">
                            <p className="cart-p">
                            <button
                                type="button"
                                className="form-control btn btn-primary submit px-3 cart-remove-btn mx-3 my-3"
                                onClick={backToSummaryHandler}
                              >
                                Back to Summary
                              </button>
                              {!cancel && !delivered ?
                              <button
                                type="button"
                                className="form-control btn btn-primary submit px-3 cart-remove-btn mx-3"
                                onClick={cancelOrderHandler}
                              >
                               Cancel Order
                              </button>:''
                            }
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {/* end here */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ViewOrderPage;
