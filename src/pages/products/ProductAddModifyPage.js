import React, { useContext } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../../store/user-context";
import ProductForm from "../../components/products/ProductForm";

const ProductAddModifyPage = (props) => {
  const { user } = useContext(UserContext);
  return <>{!user.isAdmin ? <Navigate to="/shop" /> : <ProductForm />}</>;
};

export default ProductAddModifyPage;
