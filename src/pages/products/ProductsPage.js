import React, { useState, useEffect, useContext } from "react";
import { Navigate, Link } from "react-router-dom";
import UserContext from "../../store/user-context";
import Product from "../../components/products/Products";
import './ProductPage.css';
const ProductsPage = (props) => {
  const { user } = useContext(UserContext);
  const [products, setProducts] = useState([]);
  console.log("you are here at product page");
  useEffect(() => {
    async function getAllProducts() {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/products/all`,
        {
          method: "POST",
          headers: {
            "content-type": "application/json",
          },
          body: JSON.stringify({
            isAdmin: true,
          }),
        }
      );
      const jsonResponse = await response.json();
      setProducts(jsonResponse);
    }
    getAllProducts();
  }, []);
  return (
    <>
      {!user.isAdmin ? (
        <Navigate to="/shop" />
      ) : (
        <div class="container py-5">
          <div className="row">
            <div className="card orders-card mb-2">
              <div className="card-body">
                <div className="row-reverse">
                  <div className="col link-add">
                    <Link
                      to="/products/add"
                      className="btn btn-primary pull-right px-5 "
                    >
                      Add
                    </Link>
                  </div>
                  <div className="col">
                    <h4 className="inline-block">Product List</h4>
                  </div>
                </div>
                <hr className="cart-hr" />
                <table class="table table-borderless table-orders">
                  <thead>
                    <tr>
                      <th scope="col table-orders-header">ITEMS</th>
                      <th scope="col">NAME</th>
                      <th scope="col">STOCKS(KG)</th>
                      <th scope="col">AMOUNT</th>
                      <th scope="col">ACTIVE</th>
                      <th scope="col">ACTION</th>
                    </tr>
                  </thead>
                  <tbody>
                    {products.map((product) => (
                      <Product key={product.productId} product={product} />
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default ProductsPage;
