import React, { useEffect, useState, useContext } from "react";
import { useParams, Link, useNavigate } from "react-router-dom";
import LogoWhite from "../../assets/images/logoWhite.png";
import UserContext from "../../store/user-context";
import CartContext from "../../store/cart-context";
import Swal from "sweetalert2";
import "./ShopProductPage.css";

const ShopProductPage = (props) => {
  const navigate = useNavigate();
  const { user } = useContext(UserContext);
  const { setProductsHandler} = useContext(CartContext);

  const { productId } = useParams();
  const [name, setName] = useState("");
  const [quantity, setQuantity] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [cartQuantity, setCartQuantity] = useState("");
  const [imgPath, setImgPath] = useState("");
  useEffect(() => {
    async function getProduct() {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/products/${productId}`
      );
      const jsonReponse = await response.json();
      setName(jsonReponse.name);
      setDescription(jsonReponse.description);
      setQuantity(jsonReponse.quantity);
      setPrice(jsonReponse.price);
      setImgPath(jsonReponse.img_path);
    }
    getProduct();
  }, [productId]);

  useEffect(() => {
    if (cartQuantity > quantity) {
      Swal.fire({
        title: "Stocks Exceeded",
        icon: "error",
        text: "You will exceed store inventory. Please order within limit",
      });

      setCartQuantity(quantity);
    }
  }, [cartQuantity, quantity]);

  const setCartQuantityHandler = (event) => {
    setCartQuantity(event.target.value);
   
  };

  const addProductToCartHandler = () => {
      const productObj = {
          productId: productId,
          name: name,
          description: description,
          price: price,
          cartQuantity: cartQuantity,
          stocksQuantity: quantity,
          imgPath: imgPath

      }
      
      setProductsHandler(productObj);
      Swal.fire({
        title: "Product Added",
        icon: "success",
        text: "Product added to cart. You will now be redirected to the shop page",
      });
      navigate("/shop");
     
  }

  const submitOrderHandler = async () => {
    console.log("user");
    console.log(user);

    if (user.id === null) {
      Swal.fire({
        title: "Authentication Failed",
        icon: "error",
        text: "Please login to checkout order!",
      });
      return;
    }

    if (user.isAdmin === true) {
      Swal.fire({
        title: "Authentication Failed",
        icon: "error",
        text: "Admins are not allowed to take order!",
      });
      return;
    }
    const token = localStorage.getItem("token");
    const response = await fetch(`${process.env.REACT_APP_API_URL}/orders/`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "content-type": "application/json",
      },
      body: JSON.stringify({
        products: [
          {
            productId: productId,
            price: price,
            quantity: cartQuantity,
            imgPath: imgPath,
          },
        ],
      }),
    });

    const jsonReponse = await response.json();
    console.log(jsonReponse);
    if (jsonReponse) {
      Swal.fire({
        title: "Order Received",
        icon: "success",
        text: "Your order was received. You will now be redirected to the products page",
      });
      setQuantity(jsonReponse.productQuantity);
      navigate("/shop");
      return;
    }
    if (jsonReponse.productQuantity === 0) {
      Swal.fire({
        title: "Out of Stock",
        icon: "error",
        text: "Some users may have checked-out this product! Please check on another day for re-stocking",
      });
      return;
    }

    if (jsonReponse.productQuantity !== 0) {
      Swal.fire({
        title: "Insufficient Stock",
        icon: "error",
        text: "Some users may have checked-out this product! Please adjust your order according to the new stock!",
      });
      setQuantity(jsonReponse.productQuantity);
    }
  };
  return (
    <div className="container mt-5 mb-5">
      <div className="row justify-content-center">
        <div className="col-md-12 col-lg-10">
          <div className="wrap">
            <div>
              <div className="text-wrap  form-wrap py-3 text-center  align-items-center order-md-last">
                <div className="text w-100">
                  <img src={LogoWhite} alt="" className="form-img" />
                </div>
              </div>
              <div className="d-flex flex-column-reverse flex-md-row">
                <div className="col">
                  <div className="col-md-10 col-sm-12 shop-block py-4 py-lg-5 px-0 mx-auto mx-0">
                    <div className="d-flex">
                      <div className="w-100">
                        <h3 className="mb-4">{name}</h3>
                      </div>
                    </div>
                    <form className="signin-form">
                      <div className="form-group mb-3">
                        <label className="label">Description</label>
                        <p>{description}</p>
                      </div>
                      <div className="form-group mb-3">
                        <label className="label">Stocks Quantity</label>
                        <p>{quantity} KG(s)</p>
                      </div>
                      <div className="form-group mb-3">
                        <label className="label">Price</label>
                        <p>{price} PHP/KG</p>
                      </div>
                      <div className="row">
                        <div className="form-group col-md-6 pt-5 mb-0">
                          <Link
                            to="/shop"
                            className="form-control btn btn-primary submit px-3"
                          >
                            Cancel
                          </Link>
                        </div>
                        <div className="form-group col-md-6 pt-5 mb-0">
                          {cartQuantity > 0 ? (
                            <button
                              type="button"
                              onClick={submitOrderHandler}
                              className="form-control btn btn-primary submit px-3"
                            >
                              Place Order
                            </button>
                          ) : (
                            <button
                              type="button"
                              onClick={submitOrderHandler}
                              className="form-control btn btn-primary submit px-3"
                              disabled
                            >
                              Place Order
                            </button>
                          )}
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                <div className="col">
                  <div className=" col-md-10 col-sm-12 shop-block py-4 py-lg-5 px-0 mx-auto mx-0">
                    <div className="shop-block-one">
                      <div className="inner-box">
                        <figure className="image-box product-img">
                          <img
                            src={`${process.env.REACT_APP_API_URL}/${imgPath}`}
                            alt=""
                          />
                        </figure>
                        <div className="form-group lower-content">
                          <label className="label">Rating</label>
                          <ul className="rating clearfix">
                            <li>
                              <i className="fas fa-star"></i>
                            </li>
                            <li>
                              <i className="fas fa-star"></i>
                            </li>
                            <li>
                              <i className="fas fa-star"></i>
                            </li>
                            <li>
                              <i className="fas fa-star"></i>
                            </li>
                            <li>
                              <i className="fas fa-star"></i>
                            </li>
                          </ul>
                        </div>
                        <div className="row">
                          <div className="col">
                            <div className="form-group pt-2">
                              <label className="label" htmlFor="cartQuantity">
                                quantity
                              </label>
                              <input
                                id="cartQuantity"
                                type="number"
                                min="1"
                                className="form-control col-md-8"
                                value={cartQuantity}
                                onChange={setCartQuantityHandler}
                              />
                            </div>
                          </div>
                          <div className="col">
                            <div className="form-group pt-5">
                              {cartQuantity > 0 ? (
                                <button
                                  type="button"
                                  onClick={addProductToCartHandler}
                                  className="form-control btn btn-primary submit px-3"
                                >
                                  Add to Cart
                                </button>
                              ) : (
                                <button
                                  type="button"
                                  onClick={addProductToCartHandler}
                                  className="form-control btn btn-primary submit px-3"
                                  disabled
                                >
                                  Add to Cart
                                </button>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ShopProductPage;
