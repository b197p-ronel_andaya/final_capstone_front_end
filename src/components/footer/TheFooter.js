import React, {useState} from "react";
import { Link } from "react-router-dom";
import Swal from 'sweetalert2';
import LogoWhite from "../../assets/images/logoWhite.png";

const Footer = (props) => {
  const [email, setEmail] = useState('');
  

  const setEmailHandler = (event) => {
    setEmail(event.target.value);
  }
  const newsHandler = (event) => {
    event.preventDefault();
    
      if(email.trim() === '' || !email.includes('@')){
        Swal.fire({
          title: "Email Invalid",
          icon: "error",
          text: "Please input a valid email",
        });
        return;
      }

      Swal.fire({
        title: "Thank you!",
        icon: "success",
        text: "You are now subscribed to our newsletter. Stay tune for more info, promos, and recipes",
      });
  }

  return (
    <footer className="main-footer">
      <div className="auto-container">
        <div className="footer-top">
          <div className="widget-section">
            <div className="row clearfix">
              <div className="col-lg-4 col-md-6 col-sm-12 footer-column">
                <div className="footer-widget logo-widget">
                  <figure className="footer-logo">
                    <Link to="/">
                      <img src={LogoWhite} alt="" />
                    </Link>
                  </figure>
                  <div className="text">
                    <p>
                      Fresh meat, fresh food, good health, good life. Welcome to
                      your health friendly meat and seafood store.
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-md-6 col-sm-12 footer-column">
                <div className="footer-widget contact-widget mt-0">
                  <ul className="info clearfix">
                    <li>
                      <a href="tel:639279970588">
                        <i className="flaticon-phone"></i>
                      </a>
                      <p>Call Us</p>
                      <h5>
                        <a href="tel:639279970588">+63 9279970588</a>
                      </h5>
                    </li>
                    <li>
                      <a
                        href="https://www.google.com/maps/search/sacot+elementary+school+bamban+tarlac/@15.2952585,120.5529117,17z"
                        target="_blank"
                        rel="noreferrer"
                      >
                        <i className="flaticon-maps-and-flags"></i>
                      </a>
                      <p>Address</p>
                      <a
                        href="https://www.google.com/maps/search/sacot+elementary+school+bamban+tarlac/@15.2952585,120.5529117,17z"
                        target="_blank"
                        rel="noreferrer"
                      >
                        <h5>Blk 12 Lot 24 Sacot Bamban Tarlac</h5>
                      </a>
                    </li>
                  </ul>
                  <div className="d-flex">
                    <ul className="social-links clearfix mx-auto">
                      <li>
                        <a
                          href="https://www.facebook.com/Steak-Seafood-N-More-100548602462066"
                          target="_blank"
                          rel="noreferrer"
                        >
                          <i className="fab fa-facebook-f"></i>
                        </a>
                      </li>
                      <li>
                        <a
                          href="https://www.instagram.com/raphaels_steakhouse/"
                          target="_blank"
                          rel="noreferrer"
                        >
                          <i className="fab fa-instagram"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-md-6 col-sm-12 footer-column">
                <div className="footer-widget newsletter-widget">
                  <div className="widget-title">
                    <h6>Newsletter</h6>
                  </div>
                  <div className="widget-content">
                    <p>Stay updated with our store products and recipes.</p>
                    <form onSubmit={newsHandler}
                      className="newsletter-form"
                    >
                      <div className="form-group">
                        <input
                          type="email"
                          name="email"
                          placeholder="Email"
                          value={email}
                          onChange= {setEmailHandler}
                          required=""
                        />
                        <button type="submit">
                          <i className="flaticon-paper-plane-1"></i>
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer-bottom clearfix">
          <div className="copyright pull-left">
            <h5>
              &copy;2022 CopyRight{" "}
              <Link to="/">Steak, Seafood & More</Link>. All rights
              reserved.
            </h5>
          </div>
          <ul className="footer-nav pull-right clearfix">
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/about">About</Link>
            </li>
            <li>
              <Link to="/recipes">Recipes</Link>
            </li>
            <li>
              <Link to="/contact">Contact</Link>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
