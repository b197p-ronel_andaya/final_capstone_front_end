import { useContext, useEffect } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../../store/user-context";

export default function Logout() {
  // Consume the UserContext object and destructure it to access the setUser function and unsetUser function from the Context Provider
  const { unsetUser, setUser } = useContext(UserContext);

  unsetUser();
  useEffect(() => {
    setUser({ id: null, isAdmin: null, firstName: null, lastName: null, email: null, mobileNo: null, address: null });
  }, [setUser]);

  return <Navigate to="/login" />;
}
