import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import LogoWhite from "../../assets/images/logoWhite.png";
import "./Register.css";
const Register = () => {
  const navigate = useNavigate();
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [contactNo, setContactNo] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const setFirstNameHandler = (event) => {
    setFirstName(event.target.value);
  };
  const setLastNameHandler = (event) => {
    setLastName(event.target.value);
  };
  const setEmailHandler = (event) => {
    setEmail(event.target.value);
  };
  const setContactNoHandler = (event) => {
    setContactNo(event.target.value);
  };
  const setPasswordHandler = (event) => {
    setPassword(event.target.value);
  };
  const setConfirmPasswordHandler = (event) => {
    setConfirmPassword(event.target.value);
  };
  const registrationHandler = async (event) => {
    event.preventDefault();
    if (password.trim() !== confirmPassword.trim()) {
      Swal.fire({
        title: "Password incorrect!",
        icon: "error",
        text: "Please double check your password and try again!",
      });
      return;
    }

    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/users/register`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          firstName: firstName,
          lastName: lastName,
          email: email,
          mobileNo: contactNo,
          password: password,
        }),
      }
    );

    const jsonResponse = await response.json();
    if (jsonResponse.emailExist) {
      Swal.fire({
        title: "User already Exist!",
        icon: "error",
        text: "A user with that email already exist! Please try to use a different one or login instead.",
      });
      return;
    }

    if (jsonResponse) {
      Swal.fire({
        title: "User Created!",
        icon: "success",
        text: "You will now be redirected to the login page!",
      });
      setFirstName("");
      setLastName("");
      setEmail("");
      setContactNo("");
      setPassword("");
      navigate("/login");
    }
  };

  return (
    <div className="container mt-5">
      <div className="row justify-content-center">
        <div className="col-md-12 col-lg-10">
          <div className="wrap d-md-flex mb-5">
            <div className="text-wrap p-4 p-lg-5 text-center d-flex flex-col align-items-center order-md-last">
              <div className="text w-100">
                <Link to="/">
                  <img
                    src={LogoWhite}
                    alt=""
                    title=""
                    className="w-50 pb-3"
                  ></img>
                </Link>
                <h2>Register</h2>
                <p>Do you have an existing account?</p>
                <Link to="/login" className="btn btn-white btn-outline-white">
                  Go to login
                </Link>
              </div>
            </div>
            <div className="login-wrap p-4 p-lg-5">
              <div className="d-flex">
                <div className="w-100">
                  <h3 className="mb-4">Register</h3>
                </div>
              </div>
              <form className="register-form" onSubmit={registrationHandler}>
                <div className="form-group mb-3">
                  <label className="label" htmlFor="firstName">
                    First Name
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Juan"
                    required
                    value={firstName}
                    onChange={setFirstNameHandler}
                  />
                </div>
                <div className="form-group mb-3">
                  <label className="label" htmlFor="lastName">
                    Last Name
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Dela Cruz"
                    required
                    value={lastName}
                    onChange={setLastNameHandler}
                  />
                </div>
                <div className="form-group mb-3">
                  <label className="label" htmlFor="email">
                    Email
                  </label>
                  <input
                    type="email"
                    className="form-control"
                    placeholder="juandelacruz@email.com"
                    required
                    value={email}
                    onChange={setEmailHandler}
                  />
                </div>
                <div className="form-group mb-3">
                  <label className="label" htmlFor="contactNo">
                    Contact No.
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="09099991234"
                    maxLength="11"
                    required
                    value={contactNo}
                    onChange={setContactNoHandler}
                  />
                </div>
                <div className="form-group mb-3">
                  <label className="label" htmlFor="password">
                    Password
                  </label>
                  <input
                    type="password"
                    className="form-control"
                    placeholder="Password"
                    required
                    value={password}
                    onChange={setPasswordHandler}
                  />
                </div>
                <div className="form-group mb-3">
                  <label className="label" htmlFor="confirmPassword">
                    Confirm Password
                  </label>
                  <input
                    type="password"
                    className="form-control"
                    placeholder="Password"
                    required
                    value={confirmPassword}
                    onChange={setConfirmPasswordHandler}
                  />
                </div>
                <div className="form-group pt-3 mb-0">
                  <button
                    type="submit"
                    className="form-control btn btn-primary submit px-3"
                  >
                    Register
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
