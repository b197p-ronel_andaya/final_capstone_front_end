import React from "react";
import { Link } from "react-router-dom";
import CorrectIcon from '../../assets/images/icons/correct.png';
import WrongIcon from '../../assets/images/icons/cross.png';
import EditIcon from '../../assets/images/icons/edit.png'
import './Product.css';

const Product = (props) => {

  console.log(props);
  return (
    <tr>
      <td>
        <img
          src={`${process.env.REACT_APP_API_URL}/${props.product.img_path}`}
          alt=""
          className="orders-img"
        />
      </td>
      <td><strong>{props.product.name}</strong></td>
      {props.product.quantity !== 0 ?
        <td>{props.product.quantity}</td>:
        <td className="text-danger"><strong>{props.product.quantity}</strong></td>
      }
      
      <td>{props.product.price.toFixed(2)} PHP</td>
      <td>{props.product.is_active ? <img src={CorrectIcon} alt="" className="active-product"/> : <img src={WrongIcon} alt="" className="active-product"/>} </td>
      <td>
        <Link to={`/products/edit/${props.product._id}`}><img src={EditIcon} className="active-product" alt="" /></Link>
      </td>
    </tr>
  );
};

export default Product;
