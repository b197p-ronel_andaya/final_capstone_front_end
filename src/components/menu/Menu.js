import React, { useContext } from "react";
import UserContext from "../../store/user-context";
import { Link } from "react-router-dom";
import './Menu.css';

const Menu = (props) => {
  const { user } = useContext(UserContext);

  return (
    <ul className="navigation clearfix">
      <li>
        <Link to="/">Home</Link>
      </li>
      <li>
        <Link to="/about">About</Link>
      </li>
      <li>
        <Link to="/shop">Store</Link>
      </li>
      <li>
        <Link to="/contact">Contact</Link>
      </li>

      {user.id === null ? (
        <>
          <li>
            <Link to="/login">Login</Link>
          </li>
          <li>
            <Link to="/register">Register</Link>
          </li>
        </>
      ) : (
        <>
          {user.isAdmin ? (
            <li className="dropdown"> <Link to="/products">Admin</Link>
              <ul>
                <li><Link to="/products">Products</Link></li>
                <li><Link to="/users">Users</Link></li>
              </ul>
              
            </li>
          
          ) : (
            ""
          )}
          <li>
            <Link to="/logout">Logout</Link>
          </li>
        </>
      )}
    </ul>
  );
};

export default Menu;
