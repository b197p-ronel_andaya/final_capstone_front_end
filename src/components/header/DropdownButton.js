import React from "react";

const DropdownButton = (props) => {
  const showMobileMenuHandler = () =>{
    document.body.classList.add("mobile-menu-visible");
  }
  return (
    <div className="mobile-nav-toggler" onClick={showMobileMenuHandler}>
      <i className="icon-bar"></i>
      <i className="icon-bar"></i>
      <i className="icon-bar"></i>
    </div>
  );
};

export default DropdownButton;
