import React from "react";

const Info = (props) => {
  return (
    <div className="header-top">
      <div className="auto-container">
        <div className="top-info">
          <ul className="info-list clearfix">
            <li>
              <a
                href="https://www.google.com/maps/search/sacot+elementary+school+bamban+tarlac/@15.2952585,120.5529117,17z"
                target="_blank"
                rel="noreferrer"
              >
                <i className="flaticon-location-pin"></i>
                Blk 12 Lot 24 Sacot Bamban Tarlac
              </a>
            </li>
            <li>
              <a href="mailto:steakseafoodandmore@gmail.com">
                <i className="flaticon-envelope"></i>
                steakseafoodandmore@gmail.com
              </a>
            </li>
            <li className="phone">
              <a href="tel:639279970588">
                <i className="flaticon-dial"></i>
                +63 9279970588
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Info;
