import React from "react";
import {Link} from 'react-router-dom';
import Menu from "../menu/Menu";
import SmallLogo from '../../assets/images/small-logo.png';
const TheStickyNavbar = props =>{
    return (
        <div className="sticky-header">
                <div className="auto-container">
                    <div className="outer-box clearfix">
                        <figure className="logo-box pull-left"><Link to="/"><img src={SmallLogo} alt="" /></Link></figure>
                        <div className="menu-area pull-right">
                            <nav className="main-menu clearfix">
                              <Menu />
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
    );
}

export default TheStickyNavbar;