import React, { useContext} from "react";
import { Link } from "react-router-dom";
import AppContext from "../../store/app-context";
import UserContext from "../../store/user-context";
import CartContext from "../../store/cart-context";

import "./TheRightNavbar.css";

const TheRightNavbar = (props) => {
  const ctx = useContext(AppContext);
  const { user } = useContext(UserContext);
  const { products } = useContext(CartContext);
  
  let cartQuantity = 0;
  for(const product of products){
    cartQuantity += (+product.cartQuantity);
  }

  console.log("you are on sidenavbar and here are the products");
  console.log(products);

  const showSidebarHandler = () => {
    ctx.setIsActive(true);
  };
  return (
    <ul className="menu-right-content pull-left clearfix">
      <li className="nav-box">
        <div className="nav-btn nav-toggler navSidebar-button clearfix">
          <i className="flaticon-list" onClick={showSidebarHandler}></i>
        </div>
      </li>

      <li className="cart-box">
        <Link to="/cart">
          <i className="flaticon-shopping-cart-1"></i>
          <span>{cartQuantity}</span>
        </Link>
      </li>
      {user.id !== null ? (
        <li className="user-box">
          <Link to="/orders/summary" className="d-flex">
            <i className="flaticon-user-symbol-of-thin-outline mx-2"></i>
            <p className="greetings mx-2">{`Hi ${user.firstName}`}</p>
          </Link>
        </li>
      ) : (
        ""
      )}
    </ul>
  );
};

export default TheRightNavbar;
