import React from "react";
import Menu from "../menu/Menu";

const LeftNavbar = (props) => {
  return (
    <nav className="main-menu navbar-expand-md navbar-light">
      <div
        className="collapse navbar-collapse show clearfix"
        id="navbarSupportedContent"
      >
        <Menu />
      </div>
    </nav>
  );
};

export default LeftNavbar;
