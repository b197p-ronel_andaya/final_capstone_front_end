import React, { useState, useEffect } from "react";
import {Link} from 'react-router-dom';
import Info from "./Info";
import DropdownButton from "./DropdownButton";
import MobileMenu from "../mobile-menu/MobileMenu";
import TheLeftNavbar from "./TheLeftNavbar";
import TheRightNavbar from "./TheRightNavbar";
import TheStickyNavbar from "./TheStickyNavbar";

import "./Header.css";

import CompanyLogo from "../../assets/images/logo2.png";
const Header = (props) => {
  const [fixedHeader, setFixedHeader] = useState("");

  const controlNavbar = () => {
    if (window.scrollY > 100) {
      setFixedHeader("fixed-header");
    } else {
      setFixedHeader("");
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", controlNavbar);
    return () => {
      window.removeEventListener("scroll", controlNavbar);
    };
  }, []);

  return (
    <>
      <header className={`main-header ${fixedHeader}`}>
        <Info />
        <div className="header-upper">
          <div className="auto-container">
            <div className="outer-box clearfix">
              <div className="logo-box">
                <figure className="logo">
                  <Link to="/">
                    <img src={CompanyLogo} alt="" id="logo-img"></img>
                  </Link>
                </figure>
              </div>
              <div className="menu-area pull-right">
                <DropdownButton />
                <TheLeftNavbar />
                <TheRightNavbar />
              </div>
            </div>
          </div>
        </div>
        <TheStickyNavbar />
      </header>
      <MobileMenu />
    </>
  );
};

export default Header;
