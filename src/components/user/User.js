import React, { useState, useEffect } from "react";

import CorrectIcon from "../../assets/images/icons/correct.png";
import WrongIcon from "../../assets/images/icons/cross.png";

const User = (props) => {
  const [rights, setRights] = useState(props.user.is_admin);

  const setRightsHandler = (event) => {
    if (rights) {
      setRights(false);
    } else {
      setRights(true);
    }
  };

  useEffect(() => {
    async function setUserRights() {
      let setAccess = null;
      if (rights === true) {
        setAccess = true;
      } else {
        setAccess = false;
      }

      const token = localStorage.getItem("token");
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/users/access`,
        {
          method: "PUT",
          headers: {
            Authorization: `Bearer ${token}`,
            "content-type": "application/json",
          },
          body: JSON.stringify({
            userId: props.user._id,
            isAdmin: setAccess,
          }),
        }
      );

      const jsonReponse = await response.json();
      console.log(jsonReponse);
    }
    setUserRights();
  }, [rights, props.user._id]);

  return (
    <tr>
      <td>{props.user.firstName}</td>
      <td>{props.user.lastName}</td>
      <td>{props.user.email}</td>

      <td>{props.user.mobileNo}</td>
      <td>
        {rights ? (
          <img src={CorrectIcon} alt="" className="active-product" />
        ) : (
          <img src={WrongIcon} alt="" className="active-product" />
        )}
      </td>
      <td>
        {
          <button
            type="button"
            className={
              rights ? "btn btn-warning px-2 py-1" : "btn btn-primary px-2 py-1"
            }
            onClick={setRightsHandler}
          >
            {rights ? "Remove as Admin" : "Make Admin"}
          </button>
        }
      </td>
    </tr>
  );
};

export default User;
