import React, { useContext, useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import CartContext from "../../store/cart-context";
import UserContext from "../../store/user-context";
import Swal from "sweetalert2";

const CartSummary = () => {
  const navigate = useNavigate();
  const { products } = useContext(CartContext);
  const { user } = useContext(UserContext);
  const [totalAmount, setTotalAmount] = useState(0);
  const [totalQuantity, setTotalQuantity] = useState(0);

  const setTotalAmountHandler = () => {
    setTotalAmount(0);
    for (const product of products) {
      setTotalAmount((prevState) => {
        return +prevState + +product.cartQuantity * +product.price;
      });
    }
  };

  const setTotalQuantityHandler = () => {
    setTotalQuantity(0);
    for (const product of products) {
      setTotalQuantity((prevState) => {
        return +prevState + +product.cartQuantity;
      });
    }
  };

  const proceedToCheckoutHandler = () => {
    console.log("check admin");
    console.log(user.isAdmin);
    
    if (user.id !== null && user.isAdmin === true) {
      console.log("are you an admin?");
      Swal.fire({
        title: "Not User",
        icon: "error",
        text: "This feature is for non-admin user only. Please login on your regular account!",
      });
      return;
    }
    
    if (user.id !== null) {
      navigate("/checkout");
      return;
    }  
    
    
    navigate("/login");
  };
  useEffect(() => {
    setTotalAmountHandler();
    setTotalQuantityHandler();
  }, [products]);
  return (
    <div className="container">
      <div className="row px-3">
        <div className="card orders-card mb-2">
          <div className="card-body">
            <div className="row">
              <div className="col-md-6"></div>
              <div className="col-md-6">
                <ul className="d-flex justify-content-end pt-1">
                  <li className="order-bottom-summary-list">
                    <strong>Total Quantity:</strong>
                  </li>
                  <li className="order-bottom-summary-description">
                    {totalQuantity} KGS
                  </li>
                </ul>
                <ul className="d-flex justify-content-end">
                  <li className="order-bottom-summary-list text-danger">
                    <strong>Total Amount:</strong>
                  </li>
                  <li className="order-bottom-summary-description text-danger">
                    <strong>{totalAmount.toFixed(2)} PHP</strong>
                  </li>
                </ul>
              </div>
            </div>
            <div className="row pt-5">
              <div className="col">
                <div className="form-group">
                  <p className="cart-p">
                    <button
                      type="button"
                      className="form-control btn btn-primary submit px-3 cart-remove-btn"
                      onClick={proceedToCheckoutHandler}
                    >
                      Proceed to Checkout
                    </button>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CartSummary;
