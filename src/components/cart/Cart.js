import React, { useState, useContext, useEffect} from "react";
import CartContext from "../../store/cart-context";
import Swal from 'sweetalert2';
import "./Cart.css";

let wasTouched = false;

const Cart = (props) => {
  console.log('you are inside cart');
  
  const { setProductsHandler, removeProductsHandler } = useContext(CartContext);
  const [individualCartQuantity, setIndividualCartQuantity] = useState(
    props.value.cartQuantity
  );
  const [individualCartAmount, setIndividualCartAmount] = useState(
    +props.value.price * +individualCartQuantity
  );

  const [individualStocksQuantity, setIndividualStocksQuantity] = useState(props.value.stocksQuantity);

    console.log('cartQuantity:' + individualCartQuantity);
    console.log('stocksQuantity:' + individualStocksQuantity);

  const setIndividualCartQuantityHandler = (event) => {
    
      setIndividualCartQuantity(event.target.value);
    
    console.log("testing individual cart quantity handler");
    console.log(individualCartQuantity);
    wasTouched = true;
    
  };

  const setIndividualCartAmountHandler = () => {
    setIndividualCartAmount(+individualCartQuantity * +props.value.price);
  };

  useEffect(() => {
    if(wasTouched){
      let projectQuantity = 0;
      if(individualCartQuantity > individualStocksQuantity){
        projectQuantity = +individualCartQuantity - 1;
      }else{
        projectQuantity = +individualCartQuantity;
      }
      setIndividualCartAmountHandler();
     setProductsHandler({
          productId:props.value.productId,
          cartQuantity: projectQuantity,
          fromCart: true
      })
    }

    wasTouched = false;
    //if encountered error include the individualCartQuantity
  },[individualCartQuantity]);

  const removeCartProductHandler = () => {
    removeProductsHandler({productId:props.value.productId})
  };

  useEffect(() => {
    if (individualCartQuantity > individualStocksQuantity) {
      Swal.fire({
        title: "Stocks Exceeded",
        icon: "error",
        text: "You will exceed store inventory. Please order within limit",
      });

      setIndividualCartQuantity(individualStocksQuantity);
      setIndividualCartAmount(+individualStocksQuantity * +props.value.price)
    }
  }, [individualCartQuantity, individualStocksQuantity]);

  return (
    <>
      <div className="d-flex flex-column-reverse flex-md-row">
        <div className="col">
          <div className="col-md-10 col-sm-12 shop-block py-4 py-lg-5 px-0 mx-auto mx-0">
            <div className="d-flex">
              <div className="w-100">
                <h3 className="mb-4">{props.value.name}</h3>
              </div>
            </div>

            <div className="form-group mb-3">
              <label className="label">Description</label>
              <p>{props.value.description}</p>
            </div>
            <div className="form-group mb-3">
              <label className="label">Stocks Quantity</label>
              <p>{props.value.stocksQuantity} KG(s)</p>
            </div>
            <div className="form-group mb-3">
              <label className="label">Price</label>
              <p>{props.value.price} PHP/KG</p>
            </div>
          </div>
        </div>

        <div className="col">
          <div className=" col-md-10 col-sm-12 shop-block  pt-lg-5 px-0 mx-auto mx-0">
            <div className="shop-block-one">
              <div className="inner-box">
                <figure className="image-box product-img">
                  <img
                    src={`${process.env.REACT_APP_API_URL}/${props.value.imgPath}`}
                    alt=""
                  />
                </figure>
                <div className="form-group lower-content">
                  <label className="label">Rating</label>
                  <ul className="rating clearfix">
                    <li>
                      <i className="fas fa-star"></i>
                    </li>
                    <li>
                      <i className="fas fa-star"></i>
                    </li>
                    <li>
                      <i className="fas fa-star"></i>
                    </li>
                    <li>
                      <i className="fas fa-star"></i>
                    </li>
                    <li>
                      <i className="fas fa-star"></i>
                    </li>
                  </ul>
                </div>
                <div className="row">
                  <div className="col-lg-6">
                    <div className="form-group pt-2">
                      <label className="label" htmlFor="cartQuantity">
                        quantity
                      </label>
                      <input
                        id="cartQuantity"
                        type="number"
                        min="1"
                        value={individualCartQuantity}
                        onChange={setIndividualCartQuantityHandler}
                        className="form-control"
                      />
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="form-group pt-2">
                      <label className="label">Amount</label>
                      <p className="pt-lg-2 cart-amount">
                        {individualCartAmount.toFixed(2)} PHP
                      </p>
                    </div>
                  </div>
                </div>
                <div className="row pt-2">
                  <div className="col">
                    <div className="form-group">
                      <p className="cart-p">
                        <button
                          type="button"
                          className="form-control btn btn-primary submit px-3 cart-remove-btn"
                          onClick={removeCartProductHandler}
                        >
                          Remove
                        </button>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <hr className="cart-hr"></hr>
    </>
  );
};

export default Cart;
