import React, { useContext } from "react";
import { Link } from "react-router-dom";
import AppContext from "../../store/app-context";

import "./SideInfo.css";
import LogoWhite from "../../assets/images/logoWhite.png";

const SideInfo = (props) => {
  const ctx = useContext(AppContext);

  const setNotActiveHandler = () => {
    ctx.setIsActive(false);
  };
  return (
    <div
      className={`xs-sidebar-group info-group info-sidebar ${ctx.classIsActive}`}
      onClick={setNotActiveHandler}
    >
      <div className="xs-overlay xs-bg-black"></div>
      <div className="xs-sidebar-widget">
        <div className="sidebar-widget-container">
          <div className="widget-heading">
            <span className="close-side-widget">X</span>
          </div>
          <div className="sidebar-textwidget">
            <div className="sidebar-info-contents">
              <div className="content-inner">
                <div className="logo">
                  <Link to="/">
                    <img src={LogoWhite} alt="" />
                  </Link>
                </div>
                <div className="content-box">
                  <h4>About Us</h4>
                  <p>
                    We are Steak Seafoods & More. A supplier of quality meat and
                    seafoods. To provide a good meal for your family at an
                    affordable price is our main goal. Take a minute to know
                    more about us.
                  </p>
                  <Link to="/about" className="theme-btn">
                    About Us<i className="fas fa-long-arrow-alt-right"></i>
                  </Link>
                </div>
                <div className="contact-info">
                  <h4>Contact Info</h4>
                  <ul>
                    <li>Blk 12 Lot 24 Sacot Bamban Tarlac</li>
                    <li>
                      <a href="tel:+639279970588">+63 9279970588</a>
                    </li>
                    <li>
                      <a href="mailto:steakseafoodandmore@gmail.com">
                        steakseafoodandmore@gmail.com
                      </a>
                    </li>
                  </ul>
                </div>
                <ul className="social-box d-flex justify-content-center">
                  <li className="facebook px-2">
                    <a
                      href="https://www.facebook.com/Steak-Seafood-N-More-100548602462066"
                      className="fab fa-facebook-f"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <span></span>
                    </a>
                  </li>
                  <li className="instagram px-2">
                    <a
                      href="https://www.instagram.com/raphaels_steakhouse/"
                      className="fab fa-instagram"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <span></span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SideInfo;
