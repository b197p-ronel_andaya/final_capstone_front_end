import React from "react";
import { Link } from "react-router-dom";

import "./ShopCard.css";

const ShopCard = (props) => {

  return (
    <div className="col-lg-3 col-md-4 col-sm-12 shop-block">
      <div className="shop-block-one">
        <div className="inner-box">
          <figure className="image-box product-img">
            <img src={`${process.env.REACT_APP_API_URL}/${props.product.img_path}`} alt="" />
          </figure>
          <div className="lower-content">
            <h6>
              <Link to={`/shop/product/${props.product._id}`}>
                {props.product.name}
              </Link>
            </h6>
            <ul className="rating clearfix">
              <li>
                <i className="fas fa-star"></i>
              </li>
              <li>
                <i className="fas fa-star"></i>
              </li>
              <li>
                <i className="fas fa-star"></i>
              </li>
              <li>
                <i className="fas fa-star"></i>
              </li>
              <li>
                <i className="fas fa-star"></i>
              </li>
            </ul>
            <span className="price">{props.product.price.toFixed(2)} PHP</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ShopCard;
