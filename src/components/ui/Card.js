import {Card} from 'react-bootstrap';
import './Card.css';

const CardComponent = props => {
    return (
        <Card className={`col-xl-3 col-lg-6 col-sm-8 mx-auto shadow bg-body rounded ${props.className}`}>
        <Card.Body>
            {props.children}
        </Card.Body>
      </Card>
    )
}

export default CardComponent;