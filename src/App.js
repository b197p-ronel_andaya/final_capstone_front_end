import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Header from "./components/header/Header";
import SideInfo from "./components/side-info/SideInfo";

import Logout from "./components/logout/logout";
import LoginPage from "./pages/LoginPage";
import RegisterPage from "./pages/RegisterPage";
import ShopPage from "./pages/shop/ShopPage";
import ShopProductPage from "./pages/shop/ShopProductPage";
import ProductsPage from "./pages/products/ProductsPage";
import ProductAddModifyPage from "./pages/products/ProductAddModifyPage";
import CartPage from "./pages/cart/CartPage";
import CheckOutPage from "./pages/checkout/CheckOutPage";
import AddressPage from "./pages/address/AddressPage";
import UsersPage from "./pages/users/UsersPage";
import OrderSummaryPage from "./pages/order-summary/OrderSummaryPage";
import ViewOrderPage from "./pages/order-summary/ViewOrderPage";

import AppContext from "./store/app-context";
import UserContext from "./store/user-context";
import CartContext from "./store/cart-context";
import TheFooter from "./components/footer/TheFooter";

import "./assets/css/font-awesome-all.css";
import "./assets/css/flaticon.css";
import "./assets/css/owl.css";
import "./assets/css/jquery.fancybox.min.css";
import "./assets/css/animate.css";
import "./assets/css/color.css";
import "./assets/css/style.css";
import "./assets/css/responsive.css";

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    firstName: null,
    lastName: null,
    email: null,
    mobileNo: null,
    address: null,
  });

  const [cart, setCart] = useState({});
  const [products, setProducts] = useState([]);

  const [isActive, setIsActive] = useState("");

  const unsetUser = () => {
    localStorage.removeItem("token");
  };

  const setProductsHandler = (productObj) => {
    for (const product of products) {
      if (product.productId === productObj.productId) {
        const index = products.indexOf(product);
        const newProducts = [...products];

        //add from cart
        if (productObj.fromCart) {
          newProducts[index].cartQuantity = +productObj.cartQuantity;
          setProducts(newProducts);
          return;
        }
        //end
        newProducts[index].cartQuantity =
          +product.cartQuantity + +productObj.cartQuantity;
        setProducts(newProducts);
        return;
      }
    }
    setProducts((previousProduct) => [...previousProduct, productObj]);
  };

  const removeProductsHandler = (productObj) => {
    for (const product of products) {
      if (product.productId === productObj.productId) {
        const newProducts = [...products];

        const filteredProducts = newProducts.filter(
          (product) => product.productId !== productObj.productId
        );
        setProducts(filteredProducts);
      }
    }

    if (products.length === 1) {
      localStorage.removeItem("products");
    }
  };

  const deleteProductsArrayHandler = () => {
    setProducts([]);
    localStorage.removeItem("products");
  };

  const setCartHandler = () => {
    setCart({ ...cart, products: products });
  };

  useEffect(() => {
    console.log("check products array");
    console.log(products);
  }, [products]);

  const unsetCartHandler = () => {
    localStorage.removeItem("cart");
  };

  useEffect(() => {
    if (localStorage.getItem("token")) {
      async function fetchData() {
        const response = await fetch(
          `${process.env.REACT_APP_API_URL}/users/details`,
          {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          }
        );

        const jsonResponse = await response.json();
        if (typeof jsonResponse._id !== "undefined") {
          setUser({
            id: jsonResponse._id,
            isAdmin: jsonResponse.is_admin,
            firstName: jsonResponse.firstName,
            lastName: jsonResponse.lastName,
            email: jsonResponse.email,
            mobileNo: jsonResponse.mobileNo,
            address: jsonResponse.address,
          });
        } else {
          setUser({
            id: null,
            isAdmin: null,
            firstName: null,
            lastName: null,
            email: null,
            mobileNo: null,
            address: null,
          });
        }
      }
      fetchData();
    }
  }, []);

  useEffect(() => {
    console.log("are you moving here to update localstorage?");
    if (products.length > 0) {
      localStorage.setItem("products", JSON.stringify(products));
      console.log("update the cart");
      console.log(localStorage.getItem("products"));
    }
  }, [products]);

  useEffect(() => {
    if (localStorage.getItem("products")) {
      console.log("should happen 1 only");
      setProducts(JSON.parse(localStorage.getItem("products")));
    }
  }, []);

  const setIsActiveHandler = (state) => {
    if (state === true) {
      setIsActive("isActive");
    } else {
      setIsActive("");
    }
  };

  return (
    <AppContext.Provider
      value={{
        classIsActive: isActive,
        setIsActive: setIsActiveHandler,
      }}
    >
      <UserContext.Provider
        value={{
          user,
          setUser,
          unsetUser,
        }}
      >
        <CartContext.Provider
          value={{
            cart,
            products,
            setProductsHandler,
            removeProductsHandler,
            deleteProductsArrayHandler,
            setCartHandler,
            unsetCartHandler,
          }}
        >
          <Router>
            <div className="boxed_wrapper ltr">
              <SideInfo />
              <Header />

              <Routes>
                <Route path="/" element={<ShopPage />} />
                <Route path="/login" element={<LoginPage />} />
                <Route path="/register" element={<RegisterPage />} />
                <Route path="/shop" element={<ShopPage />} />

                <Route
                  path="/shop/product/:productId"
                  element={<ShopProductPage />}
                />
                <Route path="/products" element={<ProductsPage />} />
                <Route
                  path="/products/add"
                  element={<ProductAddModifyPage />}
                />
                <Route
                  path="/products/edit/:productId"
                  element={<ProductAddModifyPage />}
                />
                <Route path="/user/address" element={<AddressPage />} />
                <Route path="/cart" element={<CartPage />} />
                <Route path="/checkout" element={<CheckOutPage />} />
                <Route path="/users" element={<UsersPage />} />
                <Route path="/orders/summary" element={<OrderSummaryPage />} />
                <Route
                  path="/orders/view/:orderId"
                  element={<ViewOrderPage />}
                />

                <Route path="/logout" element={<Logout />} />
              </Routes>
              <TheFooter />
            </div>
          </Router>
        </CartContext.Provider>
      </UserContext.Provider>
    </AppContext.Provider>
  );
}

export default App;
